import numpy as np
import tomato as tt

from tomato_rules import denguev2 as rule

# Use os cell_args para alterar parâmetros no modelo
cell_args = {
    "seed": 1,  # semente para o gerador de números aleatórios
    "heal_time": 200,
    "mosquito_lifespan": 90,
    "water_still_factor": 0.001,
    "spawn_prob": 0.001,
    "infection_prob": 1.0,
    "kamikaze_mosquito": False,
    "death_prob": 0.0,
}
cell_size = 16
board = tt.Board(rule, cell_size=cell_size)
board.start("images/city_large.png", cell_args=cell_args)
