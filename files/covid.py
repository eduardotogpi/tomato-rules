import numpy as np
import tomato as tt

from tomato_rules import covid as rule

# susceptible: 0, green
# infected: 1, red
# recovered: 3, white
# dead: 4, black

cell_args = {
    "k1": 0.3,  # infection factor for assymptomatic
    "k2": 0.6,  # infection factor for infected
    "Pc1": 0.00024038461,  # death probability for assymptomatic
    "Pc2": 0.02,  # death probability for infected
    "Pd": 0,  # 0.0579326923 general death probability
    "Pb1": 0.05,  # recovering probability for assymptomatic
    "Pb2": 0.2,  # recovering probability for infected
    "Px": 0.2,  # probability of becoming assymptomatic
}

cell_size = 6
dimensions = (100, 100)

# Probabilities of infected, assymptomatic and susceptible cells in initial
# state matrix
prob_infec = 0.001
prob_ass = prob_infec
prob_susc = 1 - prob_infec - prob_ass

state_matrix = np.zeros(dimensions)
state_matrix[tuple(i // 2 for i in dimensions)] = 2

board = tt.Board(rule, cell_size=cell_size, max_fps=20, generations=200)
board.start(
    state_matrix,
    inline=False,
    cell_args=cell_args,
)
