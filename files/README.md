# Examples

Please provide a minimal working example for your rule in this
folder, preferably one that can be directly run and visualized.

For example, check out game_of_life.py, which exemplifies the
rule of the same name:
```
import tomato as tt
from tomato.functions import utils
from tomato_rules import game_of_life as rule

cell_size = 4
dimensions = 100

state_matrix = utils.random_matrix(dimensions, 2)

board = tt.Board(rule, cell_size=cell_size)
board.start(state_matrix)
```
Also, avoid using packages and modules other than those already
provided and/or required by this package or tomato-engine.
