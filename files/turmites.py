import tomato as tt
#from tomato.functions import utils
from tomato_rules import turmites as rule
from numpy import zeros

cell_size = 4
dimensions = (80, 80)
ant_args = {
    "pos": (dimensions[0] // 2, dimensions[1] // 2),
    "dir": (0, -1),
    "name": tuple("RLLR"),
    "loop_colors": False,
}

state_matrix = zeros(dimensions, dtype='uint8')

board = tt.Board(rule, cell_size=cell_size, max_fps=120)
board.start(state_matrix, cell_args=ant_args)
