from tomato.classes import cell

"""
Author: Eduardo Lopes Dias (codeberg.org/eduardotogpi)

A version of the Game of Life that uses the triangular vertices neighborhood (LV).
"""


class Cell(cell.CellTemplate):
    # {{{
    def update(self, state_matrix):
        self.state_matrix = state_matrix

        # Dead cell
        if self.value == 0:
            if self.live_neighbors == 4:
                self.value = 1
        # Live cell
        else:
            if self.live_neighbors in (4, 5, 6):
                self.value = 1
            else:
                self.value = 0

    @property
    def neighbors(self):
        return self.triangular_vertices_neighborhood

    @property
    def live_neighbors(self):
        return sum(self.neighbors)

    @staticmethod
    def display(val):
        return (255, 255, 255) if val else (0, 0, 0)

    @staticmethod
    def from_display(val):
        return 1 if (val == (0, 0, 0)).all() else 0


# }}}
