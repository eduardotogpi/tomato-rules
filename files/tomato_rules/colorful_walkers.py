from collections import namedtuple
from random import choices

from tomato.classes import agent, cell

WalkerTuple = namedtuple("WalkerTuple", ["val", "pos"])


class CustomWalkerBackground(cell.CellTemplate):
    # {{{
    trail_colors = {
        "B": (0, 0, 1),
        "G": (0, 1, 0),
        "R": (1, 0, 0),
        "C": (0, 1, 1),
        "M": (1, 0, 1),
        "Y": (1, 1, 0),
        "W": (1, 1, 1),
    }

    @classmethod
    def simulation_start(cls, state_matrix, state_list, cell_args):
        cls.gens_to_fade = cell_args["gens_to_fade"]
        cls.color = CustomWalkerBackground.trail_colors[
            cell_args["trail_color"]
        ]

    def update(self, state_matrix, state_list):
        self.agent_list = state_list

        agent_above = self.agent_above

        if agent_above:
            self.value = 255
        else:
            self.value -= 255 // self.gens_to_fade
            if self.value < 0:
                self.value = 0

    @staticmethod
    def display(value):
        R, G, B = CustomWalkerBackground.color
        return (value * R, value * G, value * B)


# }}}


class CustomWalker(agent.AgentTemplate):
    # {{{
    walker_colors = {
        "B": (0, 0, 255),
        "G": (0, 255, 0),
        "R": (255, 0, 0),
        "C": (0, 255, 255),
        "M": (255, 0, 255),
        "Y": (255, 255, 0),
    }

    walkers_count = 0

    def __init__(self, val, pos, cell_args):
        self.lin, self.col = pos
        self.value = val  # the agent's value is its color

        self.num = CustomWalker.walkers_count
        CustomWalker.walkers_count += 1

        self.possible_dirs = cell_args["possible_dirs"][self.num]
        self.dir_weights = cell_args["dir_weights"][self.num]

    @classmethod
    def simulation_start(cls, state_matrix, state_list, cell_args):
        cls.walkers_count = 0

    def update(self, state_matrix, state_list):
        direction = choices(self.possible_dirs, weights=self.dir_weights, k=1)

        # The [0] is necessary because choices returns a list
        dirlin, dircol = direction[0]

        m, n = state_matrix.shape
        self.lin = (self.lin + dirlin) % m
        self.col = (self.col + dircol) % n

    @staticmethod
    def display(value):
        return CustomWalker.walker_colors[value]


# }}}
