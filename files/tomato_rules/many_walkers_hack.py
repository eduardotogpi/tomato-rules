from random import choice, seed

from tomato.classes import cell

"""
Author: Eduardo Lopes Dias (codeberg.org/eduardotogpi)

Another example of a "random-walker" implementation. This one is more complex as it
depends on alternating the value of a flag on each generation. This flag (the global
variable "move") determines whether the turn will be spent alternating the direction of
the "walker" automata or actually moving the walkers. 

As a result, the simulation will only happen on even (or odd) generations. This makes
having independent randomly-moving walkers possible. This implementation ended up being
very complicated as walkers have to 'plan' their moves so as to not land on the same
cell as another walker, eliminating it.
"""


class Cell(cell.CellTemplate):
    # walker: 0, 1, 2, 3 -> white
    # background: 4 -> black
    # trail: 5 ... 255 -> fading red

    def __init__(self, val, pos, cell_args):
        # {{{
        global move

        self.value = val
        self.lin, self.col = pos

        if "seed" in cell_args:
            seed(cell_args["seed"])

        move = True

    # }}}

    @classmethod
    def generation_start(cls, cell_args):
        # {{{
        # All this does is alternate the "move" flag at the start of each generation.
        global move

        move = not move

    # }}}

    def update(self, state_matrix):
        # {{{
        global move
        self.state_matrix = state_matrix

        # On move=False generations, all that will happen is the "walker" cells will
        # choose a random direction to move in the next generation, when move=True.
        if not move:
            # a walker
            if self.value < 4:
                self.value = self.next_step()

        else:
            # a walker
            if self.value < 4:
                self.value = 255
            # the black background or trails
            else:
                for idx, val in enumerate(self.neighbors):
                    if val < 4 and idx == (val + 2) % 4:
                        self.value = val
                        break

                if self.value > 4:
                    self.value -= 1

    # }}}

    @staticmethod
    def display(value):
        # {{{
        if value < 4:
            return (value * 62, 255, 255)
        else:
            return (value - 4, 0, 0)
            # return (0, 0, 0)

    # }}}

    @staticmethod
    def from_display(value):
        # {{{
        if value[1] == 255:
            return value[0] / 62
        else:
            return value[0] + 4

    # }}}

    @property
    def neighbors(self):
# {{{
        #   0
        # 3 w 1
        #   2
        return self.neumann_neighborhood
# }}}

    @property
    def second_neighbors(self):
        # {{{
        #      0
        #
        # 3    w    1
        #
        #      2
        lin, col = self.lin, self.col

        state_matrix = self.state_matrix
        m, n = state_matrix.shape
        lin, col = self.lin, self.col
        prev_lin, next_lin = lin - 2, lin + 2
        prev_col, next_col = col - 2, col + 2
        try:
            neighbors = [
                state_matrix[prev_lin, col],
                state_matrix[lin, next_col],
                state_matrix[next_lin, col],
                state_matrix[lin, prev_col],
            ]
        except IndexError:
            prev_lin %= m
            next_lin %= m
            prev_col %= n
            next_col %= n

            neighbors = [
                state_matrix[prev_lin, col],
                state_matrix[lin, next_col],
                state_matrix[next_lin, col],
                state_matrix[lin, prev_col],
            ]
        return neighbors

    # }}}

    @property
    def diagonal_neighbors(self):
        # {{{
        # 0   1
        #   w
        # 3   2
        lin, col = self.lin, self.col

        state_matrix = self.state_matrix
        m, n = state_matrix.shape
        lin, col = self.lin, self.col
        prev_lin, next_lin = lin - 1, lin + 1
        prev_col, next_col = col - 1, col + 1
        try:
            neighbors = [
                state_matrix[prev_lin, prev_col],
                state_matrix[prev_lin, next_col],
                state_matrix[next_lin, next_col],
                state_matrix[next_lin, prev_col],
            ]
        except IndexError:
            prev_lin %= m
            next_lin %= m
            prev_col %= n
            next_col %= n

            neighbors = [
                state_matrix[prev_lin, prev_col],
                state_matrix[prev_lin, next_col],
                state_matrix[next_lin, next_col],
                state_matrix[next_lin, prev_col],
            ]
        return neighbors

    # }}}

    def next_step(self):
        # {{{
        # This function chooses the next value (and therefore movement direction) for
        # the random walker. it has to take into account all cells within 1 and 2 units
        # of Neumann distance to make sure it won't choose a move that will result in it
        # landing on the same cell as another random walker.

        free_second_neighbors = set(
            x for x in range(4) if self.second_neighbors[x] >= 4
        )
        free_diag_neighbors = set(
            x
            for x in range(4)
            if self.diagonal_neighbors[x] >= 4
            and self.diagonal_neighbors[(x + 1) % 4] >= 4
        )
        free_neighbors = set(x for x in range(4) if self.neighbors[x] >= 4)
        try:
            chosen = choice(
                tuple(
                    free_neighbors.intersection(
                        free_second_neighbors, free_diag_neighbors
                    )
                )
            )
        except IndexError:
            # This will happen if the intersection of all "free_neighbors" sets is
            # empty, which means any move may result in it bonking another walker. In
            # this case it just chooses a random direction.
            return choice((0, 1, 2, 3))
        return chosen


# }}}
