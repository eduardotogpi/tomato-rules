from tomato.classes import cell
from random import choice

"""
Author: Murilo Melhem (codeberg.org/Muril-o)
"""


class Cell(cell.CellTemplate):
    # {{{
    def update(self, state_matrix):
        self.state_matrix = state_matrix
        live_neighbors = self.live_neighbors

        if live_neighbors > 7:
            self.value = 1
        elif live_neighbors > 1:
            zz = choice((0, 1))
            if zz:
                self.value = 0  # normal
            else:
                self.value = 1  # growth
        else:
            self.value = 0

    @property
    def neighbors(self):
        return self.moore_neighborhood

    @property
    def live_neighbors(self):
        return sum(self.neighbors)


# }}}
