from tomato.classes import cell

"""
Author: Eduardo Lopes Dias (codeberg.org/eduardotogpi)

A very simple totalistic rule with rulestring B1/S12345678. Produces a beautiful fractal
pattern when initialized on a point or cross matrix. I encourage you to change the
neighborhood below and initial matrix, it never fails to produce interesting patterns.
"""


class Cell(cell.CellTemplate):
    # {{{
    def update(self, state_matrix):
        self.state_matrix = state_matrix

        if self.value == 0:
            if self.live_neighbors == 1:
                self.value = 1

    @property
    def neighbors(self):
        return self.moore_neighborhood

    @property
    def live_neighbors(self):
        return sum(self.neighbors)

    @staticmethod
    def display(val):
        return (255, 255, 255) if val else (0, 0, 0)

    @staticmethod
    def from_display(val):
        return 0 if (val == (0, 0, 0)).all() else 1


# }}}
