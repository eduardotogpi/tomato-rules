import numpy as np
import tomato as tt
from tomato.functions import utils

from tomato_rules import many_walkers as rule

cell_size = 5
dimensions = (100, 100)
cell_args = {}  # put the seed here if you want to

#state_matrix = utils.random_choice_matrix(dimensions, (0, 1, 2, 3, 4))
state_matrix = 4*np.ones(dimensions)

# Placing 9 walkers
rows_array = np.array([40, 40, 40, 50, 50, 50, 60, 60, 60])
cols_array = np.array([60, 50, 40, 60, 50, 40, 60, 50, 40])
state_matrix[rows_array, cols_array] = 0

board = tt.Board(rule, cell_size=cell_size)
board.start(state_matrix, cell_args=cell_args)
