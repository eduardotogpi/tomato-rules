import numpy as np
import tomato as tt
from tomato.classes import cell

from tomato_rules import fake_news_simpler as rule

cell_size = 5
board_dimensions = (100, 100)
seed = 9

np.random.seed(seed)
state_matrix = np.random.choice(a=[0, -1, 1], size=board_dimensions, p=[0.4, 0.3, 0.3])

board = tt.Board(rule, cell_size=cell_size)
board.start(state_matrix)
