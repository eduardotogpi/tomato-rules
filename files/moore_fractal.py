import tomato as tt
from tomato.functions import utils
from tomato_rules import moore_fractal as rule

cell_size = 4
dimensions = 150

state_matrix = utils.point_matrix(dimensions, 1)

board = tt.Board(rule, cell_size=cell_size)
board.start(state_matrix)
